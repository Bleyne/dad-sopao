import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import { initialize, getFormValues } from 'redux-form'
import { hashHistory } from 'react-router'
import { logout } from '../auth/authActions'
import {reset as resetForm} from 'redux-form'

import consts from '../consts'

export function index(page, sizePerPage, filters = []) {
    return dispatch => {
        axios.post(`${consts.API_URL}/order-service/get`, { page, sizePerPage, filters })
            .then(resp => { 
                dispatch({ type: 'ORDER_SERVICE_FETCHED', payload: resp.data })
            })
            .catch(e => { 

                if (typeof(e.response.data.message) == 'object') {
                    e.response.data.message.forEach(
                        error => toastr.error('Erro', error)
                    )
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
    }
}

export function create() {
    return dispatch => {
        dispatch(initialize('orderServiceForm', { }))
        }
}

export function edit(id) {
    return dispatch => {
        axios.get(`${consts.API_URL}/order-service/${id}`)
            .then(resp => {
                dispatch(initialize('orderServiceForm', resp.data.data))
            })
            .catch(e => {
                if (e.response.status == 401) {
                    dispatch(logout())
                    hashHistory.push('/')
                } else if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
        }
}

export function post(values) {
    return submit(values, 'post')
}

export function update(values) {
    return submit(values, 'put')
}

function submit(values, method) {
    const id = values.id ? '/' + values.id : ''

    return dispatch => {
        axios[method](`${consts.API_URL}/order-service${id}`, values)
            .then(resp => {
                toastr.success('Sucesso', 'Operação Realizada com sucesso.')
                hashHistory.push('/ordem-servico')
            })
            .catch(e => {
                if (e.response.status == 401) {
                    dispatch(logout())
                    hashHistory.push('/')
                } else if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
    }
}