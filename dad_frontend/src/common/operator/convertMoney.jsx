export function convertMoneyToFloat(value) {
    value = String(value)
    if ( value.indexOf(",") !== -1) {
        value = value.replace(/[^0-9,.]/, '');
        if ( value.indexOf(",") !== -1) {
            value = value.replace(/\./g, '');
            value = value.replace(/,/g, '.');
        } else {
            value = value.replace(/,/, '.');
        }
    }
    return parseFloat(value)
}

export function moneyFormat(cell) {
    let value = isNaN(parseFloat(cell)) ? 0 : cell

    value = parseFloat(value).toLocaleString('pt-BR', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    })
    
    return 'R$ ' + value
}

export function moneyFormatMySql(cell) {
    let value = parseFloat(cell).toLocaleString('en-US', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    })
    return parseFloat(value);
}