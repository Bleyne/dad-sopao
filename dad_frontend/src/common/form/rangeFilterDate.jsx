import React, { Component } from 'react'
import moment from 'moment'


export default class RangeFilterDate extends Component {

    constructor(props) {
        super(props)
        this.isFiltered = this.isFiltered.bind(this)
    }

    componentWillMount() {
        moment.locale('pt-BR')
    }

    filter() {    
        if (this.refs.date.value || this.refs.date_to.value) {
            this.props.filterHandler({ callback: this.isFiltered })
        } else {
            this.props.filterHandler()
        }
    }

    isFiltered(targetValue) {
        const dateRef = this.refs.date.value
        const dateToRef = this.refs.date_to.value
    
        const target  = new Date(targetValue)      
        const date = (dateRef.length === 10) ? moment(dateRef, 'DD/MM/YYYY').toDate() : null
        const date_to = (dateToRef.length === 10) ? moment(dateToRef, 'DD/MM/YYYY').toDate() : null
    
        if (date) date.setHours(0, 0, 0)
        if (date_to) date_to.setHours(23, 59, 59)
        
        if (date && !date_to) {
            return target >= date
        } else if (!date && date_to) {
            return target <= date_to
        } else if (date && date_to) {
            return target >= date && target <= date_to
        } else {
            return true
        }
    }

    render() {
        return (
            <form>
                <div className='row'>
                    <div className='col-md-6'>
                        <input ref="date" type="text" alt='date' className="filter form-control" onChange={this.filter.bind(this)} placeholder="00/00/0000" />
                    </div>
                    <div className='col-md-6'>
                        <input ref="date_to" type="text" alt='date' className="filter form-control" onChange={this.filter.bind(this)} placeholder="00/00/0000" />
                    </div>
                </div>
            </form>
        )
    }

}