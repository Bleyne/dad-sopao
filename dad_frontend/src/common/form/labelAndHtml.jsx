import React, {Component} from 'react'
import Grid from '../layout/grid'
import { Editor } from 'react-draft-wysiwyg'
import htmlToDraft from 'html-to-draftjs'
import draftToHtml from 'draftjs-to-html'
import { EditorState, convertToRaw, ContentState } from 'draft-js'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import './labelAndHtml.css'

export default class LabelAndHtml extends Component {
    constructor(props) {
        super(props)
        const editorState = this.initEditorState(' ')
        this.state = {
            editorState,
            isLoaded: false
        }
        this.changeValue(editorState)
    }

    componentWillReceiveProps(nextProps) {
        let htmlValue = nextProps.input.value
        if (htmlValue !== "<p></p>\n" && this.state.isLoaded === false) {
            this.setState({
                editorState : this.initEditorState(htmlValue),
                isLoaded : true
            })
        }
    }

    initEditorState(html) {
        const contentBlock = htmlToDraft(html)
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks)
        return EditorState.createWithContent(contentState)
    }

    handleChange(editorState) {
        this.setState({editorState})
        this.changeValue(editorState)
    }

    changeValue(editorState) {
        const value = draftToHtml(convertToRaw(editorState.getCurrentContent()))
        this.props.input.onChange(value)
    }

    render() {
        const { editorState } = this.state
        const style = this.props.style || {}
        const readOnly = this.props.readOnly || false
        
        return(
            <Grid cols={this.props.cols} style={style}>
                <div className='form-group'>
                    <label htmlFor={this.props.name}>{this.props.label}</label>
                    { readOnly ? <div dangerouslySetInnerHTML={{__html: this.props.input.value}} /> :
                    <Editor
                        name={this.props.name}
                        editorState={editorState}
                        wrapperClassName="wrapper-class"
                        editorClassName="editor-class"
                        toolbarClassName="toolbar-class"
                        onEditorStateChange={(editorState) => this.handleChange(editorState)}/> 
                    }
                    {this.props.meta.touched && ((this.props.meta.error && <span className='info'>{this.props.meta.error}</span>) || (this.props.meta.warning && <span className='info'>{this.props.meta.warning}</span>))}
                </div> 
            </Grid>
        )
    }
}