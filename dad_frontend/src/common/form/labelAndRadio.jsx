import React from 'react'
import Grid from '../layout/grid'
import {Radio} from 'react-bootstrap'

export default props => {
    return(
        <Radio
            {...props.input}
            className='control'> </Radio>
    )
}