import React, { Component } from 'react'
import Grid from "../layout/grid";


export default class LabelAndInput extends Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        $('.date').mask('00/00/0000');
        $('.time').mask('00:00:00');
        $('.date_time').mask('00/00/0000 00:00:00');
        $('.cep').mask('00000-000');
        // $('.phone').mask('0000-0000');
        $('.phone').mask('(00) 0000-0000');
        $('.phone_us').mask('(000) 000-0000');
        $('.ip_address').mask('099.099.099.099');
        $('.percent').mask('##0,00%', {reverse: true});
        $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.decimal').mask('#.##0,00', {reverse: true});
        $('.decimaltrescasas').mask('###0,000', {reverse: true});

        let SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.telefone').mask(SPMaskBehavior, spOptions)
    }

    render() {
        const props = this.props
        const style = props.style || {}
        const className = props.className || ''
        const readOnly = props.readOnly || false
        const spanStyle = props.spanStyle || { paddingLeft: '10px' }
        // let $input = $('<input />').attr('alt', props.alt).val(props.input.value).setMask()
        let $input = props.input.value

        return(
            <Grid cols={props.cols} style={style}>
                <div className='form-group'>
                    { props.label ? <label htmlFor={props.name}>{props.label}</label> : '' }
                    {
                        readOnly ?
                            <span className={`${className}`} style={spanStyle}>{$input}</span>
                            :
                            <input {...props.input}
                                   className={`${className} form-control ${props.alt}`}
                                // alt={props.alt}
                                   autoFocus={props.autoFocus}
                                   tabIndex={props.tabIndex ? props.tabIndex : ''}
                                   placeholder={props.placeholder}
                                   // ref={input => { $(input).setMask() }}
                                   type={props.type} />
                    }
                    {props.meta.touched && ((props.meta.error && <span className='info'>{props.meta.error}</span>) || (props.meta.warning && <span className='info'>{props.meta.warning}</span>))}
                </div>
            </Grid>
        )
    }

}