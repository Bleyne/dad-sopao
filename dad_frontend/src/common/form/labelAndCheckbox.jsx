import React from 'react'
import Grid from '../layout/grid'
import {Checkbox} from 'react-bootstrap'

export default props => {
    const style = props.style || {}
    const className = props.className || ''
    const readOnly = props.readOnly || false
    return(
        <Grid cols={props.cols} style={style}>
            <div className='form-group'>
                {
                    <Checkbox
                        {...props.input}
                        className={`${className} form-control`}
                        checked={props.input.value}
                        style={style}
                        disabled={readOnly}
                    >{props.label}</Checkbox>

                }
                {props.meta.touched && ((props.meta.error && <span className='info'>{props.meta.error}</span>) || (props.meta.warning && <span className='info'>{props.meta.warning}</span>))}
            </div>
        </Grid>
    )
}