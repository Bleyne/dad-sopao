import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field, formValueSelector, reset } from 'redux-form'

import Modal from 'react-bootstrap/lib/Modal'
import Button from 'react-bootstrap/lib/Button'
import LabelAndInput from '../form/labelAndInput'
import LabelAndSelect from '../form/labelAndSelect'

import { close, profile } from '../../auth/authActions'


class Profile extends Component {

    render() {
        const { handleSubmit, modal } = this.props        
        const roles = this.props.role || []
        const stores = this.props.stores || []
        const branches = this.props.branches || []
        const role = this.props.role_id || false
        const required = value => (value ? undefined : 'Este campo é obrigatório')

        return (
            <Modal show={modal} onHide={this.props.close}>
                <Modal.Header closeButton>
                    <Modal.Title>Editar Perfil</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <form role='form' onSubmit={handleSubmit(this.props.profile)} >
                        <div className='row'>
                            <Field name='name' type='text' component={LabelAndInput} label='Nome*' cols='12 6' validate={[required]} readOnly={false} />
                            <Field name='email' component={LabelAndInput} type='email' label='E-mail*' cols='12 6' validate={[required]} spanStyle={{display: 'block'}} readOnly={true} />
                        </div>
                        <div className='row'>
                            <Field name='cpf' type='text' component={LabelAndInput} label='CPF*' cols='12 6' alt='cpf' validate={[required]} readOnly={true} />
                            <Field name='role_id' component={LabelAndSelect} options={roles} label='Tipo de Usuário*' cols='12 6' validate={[required]} readOnly={true} />
                        </div>
                        { ([ '2', '3', '4', 2, 3, 4 ].indexOf(role) !== -1) ?
                        <div className='row'>
                            <Field name='store_id' component={LabelAndSelect} options={stores} label='Loja*' cols='12 6' validate={[required]} readOnly={true} />
                            { ([ '3', '4', 3, 4 ].indexOf(role) !== -1) ?
                                <Field name='branch_id' component={LabelAndSelect} options={branches} label='Unidade*' cols='12 6' validate={[required]} readOnly={true} />
                            : '' }
                        </div> : '' }
                        <div className='row'>
                            <Field name='password' component={LabelAndInput} type='password' label='Senha' cols='12 6' />
                            <Field name='password_confirmation' component={LabelAndInput} type='password' label='Confirmação de Senha' cols='12 6' />
                        </div>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.close}>Cancelar</Button>
                    <Button bsStyle='primary' onClick={handleSubmit(this.props.profile)} type='submit'>Salvar</Button>
                </Modal.Footer>
            </Modal>
        )
    }

}

Profile = reduxForm({form: 'profileForm', onSubmit: profile, destroyOnUnmount: false, enableReinitialize: true})(Profile)
const selector = formValueSelector('profileForm')
const mapStateToProps = state => ({ 
    role          : state.user.role,
    stores        : state.user.stores,
    branches      : state.user.branches,
    modal         : state.auth.modal,
    role_id       : selector(state, 'role_id'),
    initialValues : { ...state.auth.user }
})
const mapDispatchToProps = dispatch => bindActionCreators({ close, profile }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Profile)