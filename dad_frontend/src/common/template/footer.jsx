import React from 'react'

export default props => (
    <footer className='main-footer'>
        <strong>
            Copyright &copy; 2018
            <a href='http://pentagrama.com.br' target='_blank'> Pentagrama Consultoria e Sistemas Ltda</a>
        </strong>
    </footer>
)