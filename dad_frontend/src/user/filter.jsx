import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field } from 'redux-form'

import LabelAndSelect from '../common/form/labelAndSelect'
import labelAndInput from '../common/form/labelAndInput'
import Row from '../common/layout/row'
import Grid from '../common/layout/grid'

import { index, getBranches } from './actions'
import LabelAndCheckbox from "../common/form/labelAndCheckbox";


class Filter extends Component {

    render() {
        const { handleSubmit, index, page, sizePerPage } = this.props
        const roles = this.props.roles || []

        roles.unshift({ id: '', value: 'Selecione...' })
        
        return (
            <form className="page-form" role='form' onSubmit={handleSubmit((filters) => index(page, sizePerPage, filters))}>
                <Row>
                    <Field name='name' component={labelAndInput} label='Nome Usuário' cols='12 6' />
                    <Field name='cpf' component={labelAndInput} label='CPF' alt="cpf" cols='12 6' combobox={false} />
                </Row>
                <Row>
                    <div className='col-lg-12 form-group'>
                        <input className='button pink br-bottom-left' type='submit' value='Buscar' />
                    </div>
                </Row>
            </form>
        )
    }
}

Filter = reduxForm({form: 'userfilter', onSubmit: index, destroyOnUnmount: false})(Filter)
const mapStateToProps = state => ({
    user          : state.auth.user,
    roles         : state.user.filterRoles
})
const mapDispatchToProps = dispatch => bindActionCreators({ index, getBranches }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Filter)
