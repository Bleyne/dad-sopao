import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Link } from 'react-router'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'

import Modal from './reset'
import Filter from './filter'

import { index, modal, resetOpenModal, exportXls, reactiveUser } from './actions'


class List extends Component {

    constructor(props) {
        super(props)
        this.handlePageChange = this.handlePageChange.bind(this)
    }

    componentWillMount() {
        const page = this.props.page || 1
        const sizePerPage = this.props.sizePerPage || 10
        const filters = this.props.filters || []

        this.props.index(page, sizePerPage, filters)
    }

    handlePageChange(page, sizePerPage) {
        const filters = this.props.filters || []

        this.props.index(page, sizePerPage, filters)
    }

    roleFormatter(cell){
        return cell == 1 ? "Administrador" : "Colaborador";
    }

    renderLinks(cell, row, props) {
        return (
            <div className="table-options">
                <OverlayTrigger placement="top" overlay={<Tooltip id="">Redefinir Senha</Tooltip>}>
                    <a className="yellow" onClick={() => props.resetOpenModal(row.id)}>
                        <i className="fa fa-lock"></i>
                    </a>
                </OverlayTrigger>
                <OverlayTrigger placement="top" overlay={<Tooltip id="">Editar</Tooltip>}>
                    <Link className="orange" to={{ pathname: "/user/" + row.id + "/edit" }}>
                        <i className="fa fa-pencil"></i>
                    </Link>
                </OverlayTrigger>
            </div>
        )
    }

    renderPaginationPanel(props) {
      return (
        <div className="col-xs-12 text-right">{ props.components.pageList }</div>
      );
    }

    render() {
        const options = {
            paginationPanel   : this.renderPaginationPanel,
            noDataText        : 'Não há registros para serem exibidos.', 
            hideSizePerPage   : true,
            onPageChange      : this.handlePageChange,
            page              : this.props.page,
            sizePerPage       : this.props.sizePerPage,
        }

        return (
            <div>
                <Filter />
                <div className='table-responsive mailbox-messages'>
                    <BootstrapTable
                        keyField='id' 
                        options={ options } 
                        data={this.props.list} 
                        fetchInfo={{ dataTotalSize: this.props.totalSize }}
                        pagination remote striped hover condensed >
                        <TableHeaderColumn dataField='name' headerTitle={false}>Nome</TableHeaderColumn>
                        <TableHeaderColumn dataField='cpf' headerTitle={false}>CPF</TableHeaderColumn>
                        <TableHeaderColumn dataField='email' headerTitle={false}>E-mail</TableHeaderColumn>
                        <TableHeaderColumn dataField='role_id' dataFormat={this.roleFormatter} headerTitle={false}>Tipo</TableHeaderColumn>
                        <TableHeaderColumn dataField='' dataFormat={ this.renderLinks } formatExtraData={ this.props } headerText='Ações'>Ações</TableHeaderColumn>
                    </BootstrapTable>
                </div>
                <Modal/>
            </div>
        )
    }
}

const mapStateToProps = state => ({ 
    list        : state.user.list,
    totalSize   : state.user.totalSize,
    page        : state.user.page,
    sizePerPage : state.user.sizePerPage,
    filters     : state.user.filters,
    user        : state.auth.user
})
const mapDispatchToProps = dispatch => bindActionCreators({ index, modal, resetOpenModal, exportXls, reactiveUser }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(List)
