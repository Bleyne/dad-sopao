import React from 'react'
import { Router, Route, IndexRoute, Redirect, hashHistory } from 'react-router'

import AuthOrApp from './authOrApp'
import UserList from '../user/index'
import UserForm from '../user/form'
import Auth from '../auth/auth'
import Dashboard from '../dashboard/index'

import OrdemServicoList from '../ordem_servico/index'
import OrdemServicoForm from '../ordem_servico/form'

export default props => (
    <Router history={hashHistory}>
        <Route path='/' component={AuthOrApp}>
            <IndexRoute component={Dashboard}/>
            <Route path='password/reset/:token' component={Auth} />
            <Route path='user' component={UserList} />

            <Route path='user/new' component={UserForm} />
            <Route path='user/:id' component={UserForm} />
            <Route path='user/:id/:edit' component={UserForm} />

            <Route path='ordem-servico' component={OrdemServicoList} />
            <Route path='ordem-servico/new' component={OrdemServicoForm} />
            <Route path='ordem-servico/:id/:edit' component={OrdemServicoForm} />
        </Route>
        <Redirect from='*' to='/' />
    </Router>
)