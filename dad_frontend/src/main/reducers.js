import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as toastrReducer } from 'react-redux-toastr'

import AuthReducer from '../auth/authReducer'
import UserReducer from '../user/reducer'
import OrdemServicoReducer from '../ordem_servico/reducer'


const rootReducer = combineReducers({
    auth           : AuthReducer,
    form           : formReducer,
    toastr         : toastrReducer,
    user           : UserReducer,
    ordemServico   : OrdemServicoReducer,

})

export default rootReducer
