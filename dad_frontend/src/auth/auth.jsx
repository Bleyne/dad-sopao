import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { reduxForm, Field } from 'redux-form'

import Row from '../common/layout/row'
import Grid from '../common/layout/grid'
import Messages from '../common/msg/messages'
import Input from '../common/form/inputAuth'
import OnlyInput from '../common/form/onlyInput'

import { Link } from 'react-router'
import { login, goForgotPassword, backLogin, forgotPassword, resetPassword, setTokenCaptcha } from './authActions'
import {loadReCaptcha, ReCaptcha} from 'react-recaptcha-google'

class Auth extends Component {

    constructor(props) {
        super(props);
        this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onForgotPassword = this.onForgotPassword.bind(this);
        this.onResetPassword = this.onResetPassword.bind(this);
        this.onResetPassword = this.onResetPassword.bind(this);
    }

    onSubmit(values, captcha) {
        this.props.login(values, captcha)
    }

    onForgotPassword(values) {
        this.props.forgotPassword(values)
    }

    onResetPassword(values) {
        this.props.resetPassword(values)
    }

    componentDidMount() {
        $(document).ready(function() {
            $('body').removeClass().addClass('hold-transition login-page')
            $('.cpf').mask('000.000.000-00', {reverse: true});
        })

        loadReCaptcha();
        if (this.captchaDemo) {
            this.captchaDemo.reset();
        }
    }

    onLoadRecaptcha() {
        if (this.captchaDemo) {
            this.captchaDemo.reset();
        }
    }

    verifyCallback(recaptchaToken) {
        this.props.setTokenCaptcha(recaptchaToken)
    }

    render() {
        const { handleSubmit } = this.props
        const loading = this.props.loading || false
        const token = this.props.params.token || false
        const renderForgotPassword = this.props.renderForgotPassword || false

        return (
            <div className='login-box'>
                <div className='login-logo'>
                    <img className="img-responsive center-block" src="img/logo.png" />
                </div>
                {
                    renderForgotPassword ?
                        <div className='login-box-body'>
                            <p className='login-box-msg'>Digite o e-mail cadastrado para receber uma nova senha</p>
                            <form onSubmit={handleSubmit(values => this.onForgotPassword(values))} style={{marginBottom: '10px'}}>
                                <Field component={OnlyInput} type='email' name='email' className="email" placeholder='E-mail' icon='envelope'/>
                                <Row>
                                    <Grid cols='12'>
                                        <button type='submit' disabled={loading} className='button yellow br-top-left btn-block'>{loading ? 'Aguarde...' : 'Enviar'}</button>
                                    </Grid>
                                </Row>
                            </form>
                            <a href="#" onClick={this.props.backLogin}>Voltar ao Login</a>
                        </div>
                    :
                        token ?
                            <div className='login-box-body'>
                                <p className='login-box-msg'>Redefinir Senha</p>
                                <form onSubmit={handleSubmit(values => this.onResetPassword(values))} style={{marginBottom: '10px'}}>
                                    <Field component={Input} type='hidden' name='token' />
                                    <Field component={Input} type='email' name='email' placeholder='E-mail' icon='envelope'/>
                                    <Field component={Input} type='password' name='password' placeholder='Senha' icon='lock' />
                                    <Field component={Input} type='password' name='password_confirmation' placeholder='Confirmação de Senha' icon='lock' />
                                    <Row>
                                        <Grid cols='12'>
                                            <button type='submit' disabled={loading} className='btn btn-primary btn-block btn-flat'>{loading ? 'Aguarde...' : 'Redefinir Senha'}</button>
                                        </Grid>
                                    </Row>
                                </form>
                                <Link to='/' title='Voltar ao Login'>Voltar ao Login</Link>
                            </div>
                        :
                            <div className='login-box-body'>
                                <p className='login-box-msg'>Bem vindo!</p>
                                <form onSubmit={handleSubmit(values => this.onSubmit(values, this.captchaDemo))} style={{marginBottom: '10px'}}>
                                    <Field component={Input} type='text' className='cpf' alt='cpf' name='cpf' placeholder='CPF' icon='user'/>
                                    <Field component={Input} type='password' name='password' placeholder='Senha' icon='lock' />
                                    <Row>
                                        <Grid cols='12'>
                                            <ReCaptcha
                                                ref={(el) => {this.captchaDemo = el;}}
                                                size="normal"
                                                data-theme="dark"
                                                render="explicit"
                                                sitekey="6LcdwacUAAAAACsMZDyc8VTtQ7-AemxsGDH2gk-M"
                                                onloadCallback={this.onLoadRecaptcha}
                                                verifyCallback={this.verifyCallback}
                                                hl={"pt-br"}
                                            />
                                        </Grid>
                                    </Row>
                                    <Row>
                                        <Grid cols='12'>
                                            <a href="#" onClick={this.props.goForgotPassword}>Esqueci minha senha</a>
                                            <button type='submit' className='button yellow br-bottom-left pull-right'>Entrar</button>
                                        </Grid>
                                    </Row>

                                </form>
                            </div>
                }
                <Messages />
            </div>
        )
    }
}

Auth = reduxForm({form: 'authForm'})(Auth)
const mapStateToProps = (state, props) => ({ 
    loading              : state.auth.loading,
    renderForgotPassword : state.auth.forgotPassword,
    initialValues        : { token: props.params.token }
})
const mapDispatchToProps = dispatch => bindActionCreators({ login, goForgotPassword, backLogin, forgotPassword, resetPassword, setTokenCaptcha }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Auth)
