import axios from 'axios'
import { initialize, getFormValues } from 'redux-form'
import { hashHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'

import consts from '../consts'

export function login(values, captchaDemo) {
    console.log(captchaDemo)
    values.grant_type = consts.API_GRANT
    values.client_id = consts.API_CLIENT
    values.client_secret = consts.API_SECRET
    
    return submit(values, `${consts.API_URL}/auth/login`, captchaDemo)
}

function submit(values, url, captchaDemo) {
    return dispatch => {
        axios.post(url, values)
            .then(resp => {
                dispatch({ type: 'AUTHENTICATE', payload: resp.data })
                axios.get(`${consts.API_URL}/user/init`)
                    .then(resp => {  
                        dispatch({ type: 'USER_LOGGED', payload: resp.data })
                    })
            })
            .catch(e => {
                captchaDemo.reset();
                if (e.response && e.response.data) {
                    toastr.error('Erro', e.response.data.message)
                } else {
                    toastr.error('Erro', 'Usuário ou senha inválidos.')
                }
            })
    }
}

export function forgotPassword(values) {
    return dispatch => {
        dispatch({ type: 'USER_LOADING', payload: true })
        axios.post(`${consts.API_URL}/user/forgot-password`, values)
            .then(resp => {
                dispatch(initialize('authForm', { }))
                dispatch({ type: 'USER_LOADING', payload: false })
                toastr.success('Sucesso', 'Enviamos uma nova senha para o seu e-mail.')
            })
            .catch(e => {
                dispatch({ type: 'USER_LOADING', payload: false })
                if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []                        
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
    }
}

export function resetPassword(values) {
    return dispatch => {
        dispatch({ type: 'USER_LOADING', payload: true })
        axios.post(`${consts.API_URL}/user/reset-password`, values)
            .then(resp => {
                dispatch(initialize('authForm', { }))
                dispatch({ type: 'USER_LOADING', payload: false })
                toastr.success('Sucesso', 'Sua senha foi redefinida!')
                hashHistory.push('/')
            })
            .catch(e => {
                dispatch({ type: 'USER_LOADING', payload: false })
                if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []                        
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
    }
}

export function logout() {
    return [
        { type: 'TOKEN_INVALIDATED', payload: false }
    ]
}

export function goForgotPassword() {
    return dispatch => {
            dispatch(initialize('authForm', { }))
            dispatch({ type: 'FORGOT_PASSWORD', payload: true })
        }
}

export function backLogin() {
    return dispatch => {
            dispatch(initialize('authForm', { }))
            dispatch({ type: 'FORGOT_PASSWORD', payload: false })
        }
}

export function editMe() {
    return dispatch => {
        axios.get(`${consts.API_URL}/api/user/roles`)
            .then(resp => {
                dispatch({ type: 'PROFILE_MODAL', payload: true })
                dispatch({ type: 'USER_INITIALIZE', payload: resp.data })                
            })
            .catch(e => {
                if (e.response.status == 401) {
                    dispatch(logout())
                    hashHistory.push('/')
                } else if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []                        
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
        }
}

export function close() {
    return { type: 'PROFILE_MODAL', payload: false }
}

export function profile(values) {
    const id = values.id ? values.id : ''

    return dispatch => {
        axios.put(`${consts.API_URL}/user/${id}`, values)
            .then(resp => {
                toastr.success('Sucesso', 'Operação Realizada com sucesso.')
                dispatch(close())
            })
            .catch(e => {
                if (e.response.status == 401) {
                    dispatch(logout())
                    hashHistory.push('/')
                } else if (e.response.data.errors) {
                    Object.keys(e.response.data.errors).map(i => {
                        let errors = e.response.data.errors[i] || []
                        errors.forEach(error => toastr.error('Erro', error))
                    })
                } else {
                    toastr.error('Erro', e.response.data.message)
                }
            })
    }
}

export function setTokenCaptcha(captcha) {
    return (dispatch, getState) => {
        console.log('teste')
        const state = getState()
        const values = getFormValues('authForm')(state) || {}
        values['gresponse'] = captcha
        dispatch(initialize('authForm', values))
    }
}