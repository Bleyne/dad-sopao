<?php

use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Create Dingo Router
$api = app(Router::class);

// Create a Dingo Version Group
$api->version('v1', ['middleware' => 'api'], function ($api) {

    $api->post('/user/reset-password', [
        'as' => 'user.reset-site',
        'uses' => 'App\\Api\\V1\\Controllers\\UserController@reset',
    ]);

    $api->group(['prefix' => 'auth'], function ($api) {
        $api->post('register', [
            'as' => 'register',
            'uses' => 'App\\Api\\V1\\Controllers\\RegisterController@register',
        ]);

        $api->post('login', [
            'as' => 'login',
           // 'uses' => 'Laravel\\Passport\\Http\\Controllers\\AccessTokenController@issueToken',
            'uses' => 'App\\Api\\V1\\Controllers\\AuthController@login',
        ]);

        $api->get('logout', [
            'middleware' => 'auth:api',
            'as' => 'logout',
            'uses' => 'App\\Api\\V1\\Controllers\\LogoutController@logout',
        ]);

        $api->post('recovery', [
            'as' => 'password.email',
            'uses' => 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail',
        ]);
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');
    });

    $api->post('/user/forgot-password', [
        'as' => 'users.resetpasswordForm',
        'uses' => 'App\\Api\\V1\\Controllers\\UserController@sendResetLinkEmail',
    ]);

    // Protected routes
    $api->group(['middleware' => 'auth:api'], function ($api) {
        $api->get('protected', function () {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.',
            ]);
        });

        $api->post('users', [
            'as' => 'users.index',
            'uses' => 'App\\Api\\V1\\Controllers\\UserController@index',
        ]);

        $api->get('users/{user}', [
            'as' => 'users.show',
            'uses' => 'App\\Api\\V1\\Controllers\\UserController@show',
        ]);

        //Usuários
        $api->post('usuarios', [
            'as' => 'users.create',
            'uses' => 'App\\Api\\V1\\Controllers\\UserController@create',
        ]);
        $api->put('usuarios/{user}', [
            'as' => 'users.update',
            'uses' => 'App\\Api\\V1\\Controllers\\UserController@update',
        ]);
        $api->post('/user/reset/{id}', [
            'as' => 'users.resetpassword',
            'uses' => 'App\\Api\\V1\\Controllers\\UserController@sendLinkResetPassword',
        ]);

        //Ordem Servico
        $api->post('order-service/get', [
            'as' => 'users.index',
            'uses' => 'App\\Api\\V1\\Controllers\\OrderServiceController@index',
        ]);

        $api->get('order-service/{orderservice}', [
            'as' => 'orderservice.show',
            'uses' => 'App\\Api\\V1\\Controllers\\OrderServiceController@show',
        ]);

        $api->post('order-service', [
            'as' => 'orderservice.create',
            'uses' => 'App\\Api\\V1\\Controllers\\OrderServiceController@create',
        ]);

        $api->put('order-service/{orderservice}', [
            'as' => 'orderservice.update',
            'uses' => 'App\\Api\\V1\\Controllers\\OrderServiceController@update',
        ]);

    });
});
