<?php

namespace App\Api\V1\Transformers;

use App\Models\ServiceOrder;
use League\Fractal\TransformerAbstract;

class ServiceOrderTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include.
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include.
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * @property string name
     * @property string email
     */
    public function transform(ServiceOrder $serviceOrder)
    {
        return [
            'id' => $serviceOrder->id,
            'user_id' => $serviceOrder->user_id,
            'description' => $serviceOrder->description,
            'status' => $serviceOrder->status,
            'created_at' => $serviceOrder->created_at,
            'updated_at' => $serviceOrder->updated_at,
        ];
    }
}
