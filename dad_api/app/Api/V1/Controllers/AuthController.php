<?php

namespace App\Api\V1\Controllers;

use App\Models\User;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;

class AuthController extends AccessTokenController
{
    use AuthenticatesUsers;

    protected $maxAttempts = 3; // Tentativas
    protected $decayMinutes = 1; // Tempo ban

    public function login(Request $request)
    {

        $recaptcha = $request->gresponse;

        $client = new Client([
            'base_uri' => 'https://google.com/recaptcha/api/',
            'timeout' => 2.0
        ]);

        $response = $client->request('POST', 'siteverify', [
            'query' => [
                'secret' => '6LcdwacUAAAAAIEnnWwu5Sg2wP52L3UmQzQqTJIE',
                'response' => $recaptcha]]);
        $result = json_decode($response->getBody());
        if ($result->success != true) {
            return response()->json([
                'code'    => 422,
                'message' => 'Houve um erro de verificação captcha'
            ], 422);
        }

        if ($this->hasTooManyLoginAttempts($request))
        {
            $this->fireLockoutEvent($request);

            return response()->json(['message' => 'Tentativa de login excedida. Tente novamente em alguns minutos'], 401);
        }

        $user = null;
        if ($this->validCredentials($request,$user))
        {
            $request->request->add(['username' => $user->email]);
            $this->clearLoginAttempts($request);
            $psr7Factory = new DiactorosFactory();
            $psrRequest = $psr7Factory->createRequest($request);
            $tokenResponse = parent::issueToken($psrRequest);
            return $tokenResponse;
        }
        else
        {
            $this->incrementLoginAttempts($request);

            return response()->json(['message' => 'Credenciais inválidas'], 401);
        }
    }

    public function validCredentials(Request $request, &$user) {
        $user = User::where("cpf", $request->cpf)->first();
        if($user){
            if (Hash::check($request->password,$user->password)) {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}
