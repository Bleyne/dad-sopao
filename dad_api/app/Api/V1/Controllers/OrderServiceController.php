<?php

namespace App\Api\V1\Controllers;

use App\Models\ServiceOrder;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Api\V1\Transformers\ServiceOrderTransformer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Api\V1\Requests\ServiceOrderCreateRequest;
use Illuminate\Http\Request;
use Auth;

class OrderServiceController extends Controller
{

    public function index(Request $request) {
        $filters = $request->get('filters', []);

        $serviceOrders = ServiceOrder::join('users', 'users.id', '=', 'service_orders.user_id')
            ->select(
                'service_orders.*',
                'users.name'
            )
        ->orderBy('service_orders.id', 'ASC');

        $serviceOrders = $serviceOrders->paginate($request->sizePerPage);

        return response()->json([
            'code'    => 201,
            'message' => '',
            'data' => [
                'items'        => $serviceOrders->items(),
                'totalSize'    => $serviceOrders->total(),
                'page'         => $serviceOrders->currentPage(),
                'sizePerPage'  => $request->sizePerPage,
                'filters'      => $filters
            ]
        ], 201);
    }

    public function show($id)
    {
        $fractal = new Manager();

        $fractal->parseIncludes(Input::get('include', ''));

        if (! $serviceOrders = ServiceOrder::find($id)) {
            throw new NotFoundHttpException();
        }

        $serviceOrders = new Item($serviceOrders, new ServiceOrderTransformer());

        return $fractal->createData($serviceOrders)->toArray();
    }

    public function create(ServiceOrderCreateRequest $request)
    {
        $serviceOrder = new ServiceOrder($request->all() + ['user_id' => Auth::id()]);
        if (! $serviceOrder->save()) {
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok',
        ], 201);
    }

    public function update(ServiceOrderCreateRequest $request, $id) {
        $serviceOrder = ServiceOrder::find($id);

        if (empty($serviceOrder)) {
            return response()->json([
                'code'    => 403,
                'message' => 'Usuário não encontrado.',
                'data'    => [ ]
            ], 403);
        }

        try {
            $serviceOrder->fill($request->all());
            $serviceOrder->save();
        } catch(\Exception $e) {
            return response()->json([
                'code'    => 403,
                'message' => 'Ops... Houve um erro ao atualizar o registro.',
                'data'    => $e->getTraceAsString()
            ], 403);
        }

        return response()->json([
            'code'    => 200,
            'message' => 'Registro atualizado com sucesso!',
            'data'    => [ 'item' => $serviceOrder ]
        ], 200);
    }

}
