<?php

namespace App\Api\V1\Controllers;

use App\Models\User;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Api\V1\Transformers\UserTransformer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Api\V1\Requests\UserCreateRequest;
use App\Api\V1\Requests\UserUpdateRequest;
use App\Api\V1\Requests\ForgotPassword;
use App\Api\V1\Requests\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function index(Request $request) {
        $filters = $request->get('filters', []);

        $users = User::orderBy('users.name', 'ASC');

        if (isset($filters['cpf']) && !empty($filters['cpf'])) {
            $users->where('users.cpf', $filters['cpf']);
        }
        if (isset($filters['name']) && !empty($filters['name'])) {
            $users->where('users.name', 'like', "%{$filters['name']}%");
        }

        $users = $users->paginate($request->sizePerPage);

        return response()->json([
            'code'    => 201,
            'message' => '',
            'data' => [
                'items'        => $users->items(),
                'totalSize'    => $users->total(),
                'page'         => $users->currentPage(),
                'sizePerPage'  => $request->sizePerPage,
                'filters'      => $filters
            ]
        ], 201);
    }

    public function show($id)
    {
        $fractal = new Manager();

        $fractal->parseIncludes(Input::get('include', ''));

        if (! $user = User::find($id)) {
            throw new NotFoundHttpException();
        }

        $user = new Item($user, new UserTransformer());

        return $fractal->createData($user)->toArray();
    }

    public function create(UserCreateRequest $request)
    {
        $user = new User($request->all());
        if (! $user->save()) {
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok',
        ], 201);
    }

    public function update(UserUpdateRequest $request, $id) {
        $user = User::find($id);

        if (empty($user)) {
            return response()->json([
                'code'    => 403,
                'message' => 'Usuário não encontrado.',
                'data'    => [ ]
            ], 403);
        }

        try {
            $user->fill($request->all());
            $user->save();
        } catch(\Exception $e) {
            return response()->json([
                'code'    => 403,
                'message' => 'Ops... Houve um erro ao atualizar o registro.',
                'data'    => $e->getTraceAsString()
            ], 403);
        }

        return response()->json([
            'code'    => 200,
            'message' => 'Registro atualizado com sucesso!',
            'data'    => [ 'item' => $user ]
        ], 200);
    }

    public function sendResetLinkEmail(ForgotPassword $request) {
        $response = Password::broker()->sendResetLink($request->only('email'));

        if ($response == Password::RESET_LINK_SENT) {
            return response()->json([
                'code'    => 201,
                'message' => trans($response),
                'data'    => [ ]
            ], 201);
        }

        return response()->json([
            'code'    => 403,
            'message' => trans($response),
            'data'    => [ ]
        ], 403);
    }

    public function sendLinkResetPassword($id) {
        $user = User::find($id);

        if (empty($user)) {
            return response()->json([
                'code'    => 403,
                'message' => 'Usuário não encontrado.',
                'data'    => [ ]
            ], 403);
        }

        $response = Password::broker()->sendResetLink([ 'email' => $user->email ]);
        if ($response == Password::RESET_LINK_SENT) {
            return response()->json([
                'code'    => 201,
                'message' => trans($response),
                'data'    => [ ]
            ], 201);
        }

        return response()->json([
            'code'    => 403,
            'message' => trans($response),
            'data'    => [ ]
        ], 403);
    }

    public function reset(ResetPassword $request) {
        $response = Password::broker()->reset(
            $this->credentials($request), function($user, $password) {
            $this->resetPassword($user, $password);
        }
        );

        if ($response == Password::PASSWORD_RESET) {
            return response()->json([
                'code'    => 201,
                'message' => trans($response),
                'data'    => [ ]
            ], 201);
        }

        return response()->json([
            'code'    => 403,
            'message' => trans($response),
            'data'    => [ ]
        ], 403);
    }

    protected function credentials(ResetPassword $request) {
        return $request->only('email', 'password', 'password_confirmation', 'token');
    }

    protected function resetPassword($user, $password) {
        $user->password = $password;
        $user->setRememberToken(Str::random(60));
        $user->save();
        event(new PasswordReset($user));
    }
}
