<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'cpf'   => 'required|max:14|cpf|formato_cpf|unique:users',
            'role_id'   => 'required',
        ];
    }

    public function attributes() {
        return [
            'cpf'       => 'CPF',
            'name'      => 'Nome',
            'email'     => 'E-mail',
            'password'  => 'Senha',
            'role_id'  =>  'Tipo de Usuário',
        ];
    }
}
