<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->get('id');

        return [
            'name' => 'required|string|max:255',
            'email'   => 'required|email|unique:users,email,' . $id . '|max:191',
            'cpf'     => 'unique:users,cpf,' . $id . '|max:14|cpf|formato_cpf',
            'role_id'   => 'required',
        ];
    }

    public function attributes() {
        return [
            'cpf'       => 'CPF',
            'name'      => 'Nome',
            'email'     => 'E-mail',
            'role_id'  =>  'Tipo de Usuário',
        ];
    }
}
