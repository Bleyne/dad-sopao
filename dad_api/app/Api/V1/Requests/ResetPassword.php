<?php
/*
 * @version $Revision$
 * @author $Author$
 * @since $Date$
 */
namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;


class ResetPassword extends FormRequest {
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email'    => 'required|email|max:191',
            'password' => 'required|min:6|confirmed'
        ];
    }
    
    public function attributes() {
        return [
            'email'    => 'E-mail',
            'password' => 'Senha'
        ];
    }
    
}
