<?php

namespace App\Api\V1\Requests;

use Dingo\Api\Http\FormRequest;

class ServiceOrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|string|max:500',
            'status' => 'required|int',
            'user_id' => 'int',
        ];
    }

    public function attributes() {
        return [
            'user_id'       => 'Usuário',
            'description'   => 'Descrição',
            'status'        => 'Status'
        ];
    }
}
