<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use App\Models\Traits\Method\UserMethod;
use Illuminate\Notifications\Notifiable;
use App\Models\Traits\Attribute\UserAttribute;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ServiceOrder extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'status', 'description'
    ];
}
