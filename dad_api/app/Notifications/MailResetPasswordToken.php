<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class MailResetPasswordToken extends Notification
{
    use Queueable;

    public $token;

    /**
     * Create a new notification instance.
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via()
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail()
    {
       /* return (new MailMessage())
            ->subject('Reset your password')
            ->line('You are receiving this email because we received a password reset request for your account.')
            ->line('Copy the code below.')
            ->line($this->token)
            ->line('If you did not request a password reset, no further action is required.');*/

        return (new MailMessage())
            ->subject('Redefinir Senha')
            ->line('Você está recebendo este e-mail porque recebemos uma solicitação de redefinição de senha para sua conta.')
            ->action('Redefinir Senha', url(env('FRONTEND_URL')."/password/reset/".$this->token))
            ->line('Se você não solicitou uma redefinição de senha, nenhuma outra ação será necessária.');
    }
}
