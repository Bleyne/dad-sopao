<?php

use App\Models\User;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {

        Model::unguard();

        DB::table('roles')->delete();
        Role::create(['description' => 'Administrator']);
        Role::create(['description' => 'Guest']);

        DB::table('users')->delete();
        User::create([
            'name'     => 'Bruno Leyne da Silva',
            'email'    => 'brunoleyne@gmail.com',
            'password' => '123456',
            'cpf' => '086.308.816-31',
            'role_id' => 1,
        ]);

        Model::reguard();
//        factory(App\Models\User::class, 20)->create()->each(function ($user) {
//            $user->save();
//        });
    }
}
